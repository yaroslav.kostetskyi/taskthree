package com.slawacoste.app;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

public class Initializer {

    private static final Logger logger = LoggerFactory.getLogger(Initializer.class);

    private static ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    private static Validator validator = factory.getValidator();

    private final static int FIRST_TYPE_ID = 1;
    private final static int LAST_TYPE_ID = 10;
    private final static String[] CITIES = {"Kyiv", "Kharkiv", "Lviv", "Odessa", "Kropyvnytskiy", "Izmail", "Zhitomyr"};
    private final static int MALLS_NUMBER = 20;
    private final static int RANDOM_NUMBER_BOUND = 100010;
    private final static int GOODS_NUMBER = 100000;

    static void init(ArrayList<Good> goods, ArrayList<Type> types, ArrayList<Mall> malls) {
        initGoods(goods);
        initTypes(types);
        initMalls(malls);
    }

    private static void initGoods(ArrayList<Good> goods) {
        int[] nameIndexes = makeNameIndexes();
        Good good;
        Set<ConstraintViolation<Good>> constraintViolations;
        String namePrefix = "g";
        int type_id;
        int notValidated = 0;
        for (int i = 0; i < nameIndexes.length; i++) {
            good = new Good();
            good.setGood_id(i + 1);
            good.setName(namePrefix + nameIndexes[i]);
            type_id = FIRST_TYPE_ID + (int) (Math.random() * LAST_TYPE_ID);
            good.setType(type_id);
            constraintViolations = validator.validate(good);
            if (constraintViolations.size() == 0) {
                goods.add(good);
            } else {
                logger.info("Not validated: " + constraintViolations.iterator().next().getMessage() +
                        " but name length (size) = " + good.getName().length() + ", name: " + good.getName());
                notValidated++;
            }
            if (goods.size() == GOODS_NUMBER) {
                logger.info("Added " + GOODS_NUMBER + " valid goods into ArrayList");
                break;
            }
        }
        logger.info("Not validated = " + notValidated + " goods");

    }

    private static int[] makeNameIndexes() {
        int[] nameIndexes = ThreadLocalRandom.current().ints(0, RANDOM_NUMBER_BOUND).distinct().
                limit(RANDOM_NUMBER_BOUND).toArray();
        logger.info("Created " + RANDOM_NUMBER_BOUND + " unique nameIndexes");
        return nameIndexes;
    }

    private static void initTypes(ArrayList<Type> types) {
        Type type;
        for (int i = FIRST_TYPE_ID; i <= LAST_TYPE_ID; i++) {
            type = new Type();
            type.setType_id(i);
            type.setName("type" + i);
            types.add(type);
        }
        logger.info("Created " + LAST_TYPE_ID + "types");
    }

    private static void initMalls(ArrayList<Mall> malls) {
        Mall mall;
        int cityIndex;
        for (int i = 1; i <= MALLS_NUMBER; i++) {
            mall = new Mall();
            mall.setMall_id(i);
            cityIndex = (0 + (int) (Math.random() * (CITIES.length - 1)));
            mall.setCity(CITIES[cityIndex]);
            mall.setAddress(CITIES[cityIndex] + i);
            malls.add(mall);
        }
        logger.info("Created " + MALLS_NUMBER + "malls");
    }

}

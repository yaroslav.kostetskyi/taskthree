package com.slawacoste.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Inserter {

    private static final Logger logger = LoggerFactory.getLogger(Inserter.class);

    private final static int BATCH_GOODS_SIZE = 1000;

    static void insertTypes(ArrayList<Type> types, PreparedStatement insertTypes) throws SQLException {
        for (Type type : types) {
            insertTypes.setString(1, type.getName());
            insertTypes.addBatch();
        }
        insertTypes.executeBatch();
    }

    static void insertGoods(ArrayList<Good> goods, PreparedStatement insertGoods) throws SQLException {
        Long startTime = System.currentTimeMillis();
        int countGoods = 0;
        for (Good good : goods) {
            insertGoods.setString(1, good.getName());
            insertGoods.addBatch();
            countGoods++;
            if (countGoods % BATCH_GOODS_SIZE == 0) {
                insertGoods.executeBatch();
            }
        }
        // execute the remaining queries
        insertGoods.executeBatch();
        Long finishTime = System.currentTimeMillis();
        Long insertionTime = finishTime-startTime;
        logger.info("Time of the insertion of "+goods.size()+ " is "+insertionTime+" ms");
    }

    static void insertMalls(ArrayList<Mall> malls, PreparedStatement insertMalls) throws SQLException {
        for (Mall mall : malls) {
            insertMalls.setString(1, mall.getCity());
            insertMalls.setString(2, mall.getAddress());
            insertMalls.addBatch();
        }
        insertMalls.executeBatch();
    }

    static void insertAmountOfGoods(Statement insertAmountOfGoods) throws SQLException {
        String insertIntoStores = "UPDATE Stores SET amount = 100";
                //CRYPT_GEN_RANDOM(2) % 10000";
                //abs(checksum(NewId()) % 10000)";
        insertAmountOfGoods.executeUpdate(insertIntoStores);
    }

}

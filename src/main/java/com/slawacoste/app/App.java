package com.slawacoste.app;

import java.util.ArrayList;
import java.sql.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hello world!
 */
public class App {
    private static final Logger logger = LoggerFactory.getLogger(App.class);

    /*
        private final static String DB_HOST = "mydb.ceivs8cqk357.us-east-1.rds.amazonaws.com";
        private final static String DB_PORT = "5432";
        private final static String DB_NAME = "myDB";
        private final static String DB_URL = "jdbc:postgresql://" + DB_HOST + ":" + DB_PORT + "/" + DB_NAME;
        private final static String DB_USER = "master";
        private final static String DB_PASSWD = "Julia1980";
    */

    private final static String DB_HOST = "testdb.ceivs8cqk357.us-east-1.rds.amazonaws.com";
    private final static String DB_PORT = "5432";
    private final static String DB_NAME = "testdb";
    private final static String DB_URL = "jdbc:postgresql://" + DB_HOST + ":" + DB_PORT + "/" + DB_NAME;
    private final static String DB_USER = "master";
    private final static String DB_PASSWD = "test1111";

    private static ArrayList<Good> goods = new ArrayList<>(1000000);
    private static ArrayList<Type> types = new ArrayList<>();
    private static ArrayList<Mall> malls = new ArrayList<>();

    public static void main(String[] args) {
        Initializer.init(goods, types, malls);

        String insertIntoGoods = "INSERT INTO Goods (name) VALUES (?)";
        String insertIntoTypes = "INSERT INTO Types (name) VALUES (?)";
        String insertIntoMalls = "INSERT INTO Malls (city, address) VALUES (?,?)";
        String insertIntoStores = "UPDATE Stores SET amount = (?)";
        String dropTables = "DROP TABLE IF EXISTS Types, Goods, Malls, Stores";

        // Open a connection
        try (Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWD);
             Statement statement = connection.createStatement();
             PreparedStatement insertTypes = connection.prepareStatement(insertIntoTypes);
             PreparedStatement insertGoods = connection.prepareStatement(insertIntoGoods);
             PreparedStatement insertMalls = connection.prepareStatement(insertIntoMalls)) {
            logger.info("Connected with DB");

            //delete tables
            statement.executeUpdate(dropTables);
            logger.info("Deleted tables TYPES, GOODS, MALLS, STORES");

            TableMaker.makeTables(statement);
            logger.info("Made tables TYPES, GOODS, MALLS");

            connection.setAutoCommit(false);

            //Insert types
            Inserter.insertTypes(types, insertTypes);

            //Insert goods
            Inserter.insertGoods(goods, insertGoods);

            //Insert malls
            Inserter.insertMalls(malls, insertMalls);

            //Create stores
            TableMaker.makeStoresTable(statement);
            logger.info("Made table STORES");
            //Insert data into stores
            Inserter.insertAmountOfGoods(statement);

            connection.commit();

            String answer = "SELECT Stores.mall_id, SUM(Stores.amount) FROM Stores " +
                    "NATURAL JOIN Goods " +
                    "NATURAL JOIN Types " +
                    "WHERE Types.name = 'type2' " +
                    "GROUP BY mall_id ORDER BY SUM(amount) DESC LIMIT(1)";

            ResultSet result = statement.executeQuery(answer);

            while (result.next()) {
                String mallID = result.getString(1);
                String sum = result.getString(2);
                System.out.println("mall: " + mallID + ", sum: " + sum);
            }


            //delete tables
            statement.executeUpdate(dropTables);
            logger.info("Deleted tables TYPES, GOODS, MALLS, STORES");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
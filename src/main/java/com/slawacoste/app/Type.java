package com.slawacoste.app;

public class Type {

    private int type_id;

    private String name;

    public Type() {

    }

    public Type(int type_id, String name) {
        this.type_id = type_id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType_id() {
        return type_id;
    }

    public void setType_id(int type_id) {
        this.type_id = type_id;
    }

}

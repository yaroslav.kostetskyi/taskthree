package com.slawacoste.app;

import java.util.ArrayList;

public class Mall {

    private int mall_id;

    private String city;

    private String address;

    public Mall() {

    }

    public Mall(int mall_id, String city, String address) {
        this.mall_id = mall_id;
        this.city = city;
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String name) {
        this.city = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getMall_id() {
        return mall_id;
    }

    public void setMall_id(int mall_id) {
        this.mall_id = mall_id;
    }
}

package com.slawacoste.app;

import jakarta.validation.constraints.Size;

public class Good {

    private int good_id;

    @Size(min = 3)
    private String name;

    private int type;

    private String description;

    private double price;

    public Good() {

    }

    public Good(int good_id, String name, int type, String description, double price) {
        this.good_id = good_id;
        this.name = name;
        this.type = type;
        this.description = description;
        this.price = price;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getGood_id() {
        return good_id;
    }

    public void setGood_id(int good_id) {
        this.good_id = good_id;
    }
}
